/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.algorithmdesignmidterma17;

import java.util.Scanner;

/**
 *
 * @author ADMIN
 */
public class A_1_7 {

    public static void main(String[] args) {

        Scanner kb = new Scanner(System.in);

        int c = kb.nextInt();
        int n = kb.nextInt();
        int[] A = new int[n];
        for (int i = 0; i < n; i++) {
            A[i] = kb.nextInt();
        }

//        int c = 12;
//        int[] A = new int[]{1, 2, 3, 4, 5, 6, 7};

        showResultCheckSumC(A, c, n);

    }

    private static void showResultCheckSumC(int[] A, int c, int n) {
        // show result if A[i]+A[j]==c >> YES
        // else if A[i]+A[j]!=c >> NO
        if (checkSumC(A, c, n)) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }

    // check result of A[i]+A[j]==c >> true[YES]/false[NO]
    private static boolean checkSumC(int[] A, int c, int n) {
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (A[i] + A[j] == c) {
//                    System.out.println("i = " + A[i] + ", j = " + A[j]);
                    return true;
                }
            }
        }
        return false;
    }

}
